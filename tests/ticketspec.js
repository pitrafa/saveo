define([
  'jquery',
  'models/config',
  'models/servicerequest',
  'views/index',
  'views/requestupdate',
  'app',
  'settings'
], function(jq, Config, Ticket, IndexView, TicketView, app, settings){

  'use strict';

  var index, model;

  describe('ticket view', function () {

    var org_id, ticket_id;
    var ticket
    var server;

    var setFakeIndex = function(org_id, ticket_id) {
      model = new Config({
        $org_id:org_id, $ticket_id:ticket_id,
        $org_name:'["0038 - Corp.Aerop. del Este. SAS"]'
      });
      index = new IndexView({model:model});
      index.setElement(jq('body'));
      index.render();
    };

    beforeEach(function() {
      ticket = new Ticket({ticket_id:'12588'});
      server = sinon.fakeServer.create();

      server.respondWith("GET", ticket.url(),
        [404, {"Content-Type": "application/json"},
        '{}']);
    });

    afterEach(function () {
      server.restore();
    });

    // 1. Construction stage
    describe('when constructed', function () {

      describe('without model', function () {
        it('should throw error', function () {
          expect(function () {
            new TicketView();
          }).toThrow(new Error('model is required'));
        });
      });

    });

    // 2. Rendering stage
    describe('when rendered', function () {

      xdescribe('if organization is servicio al cliente', function(){
        it('should show the button', function(){
          org_id = '[31]';
          ticket_id = '12588';
          setFakeIndex(org_id, ticket_id);
          app.start();

          server.respond();

          expect(window.$org_id).toEqual(settings.orgEnabled);
          expect(jq('.btn-aviso')).toExist();
        });
      });

      xdescribe('if organization is not servicio al cliente', function(){
        it('should not show the button', function(){
          org_id = '[39]';
          ticket_id = '12588';
          setFakeIndex(org_id, ticket_id);
          app.start();

          server.respond();

          expect(window.$org_id).not.toBe(settings.orgEnabled);
          expect(jq('.btn-aviso')).not.toExist();
        });
      });

      xdescribe('if the ticket has no aviso', function(){
        it('should show the button', function(){
          org_id = '[31]';
          ticket_id = '12588';
          setFakeIndex(org_id, ticket_id);
          app.start();

          server.respond();

          expect(window.$org_id).toEqual(settings.orgEnabled);
          expect(jq('.btn-aviso')).toExist();
        });
      });

      xdescribe('if the ticket has aviso', function(){
        it('should show the info about the ticket', function(){

          server.respondWith("GET", ticket.url(),
            [200, {"Content-Type": "application/json"},
            '{"aviso_id": "000000514737", "estado_aviso": "RPSC", "orden_id": "666"}']);

          org_id = '[31]';
          ticket_id = '12588';
          setFakeIndex(org_id, ticket_id);
          app.start();

          server.respond();

          expect(window.$org_id).toEqual(settings.orgEnabled);
          expect(jq('table')).toExist();
        });
      });

    });

  });

});