define(function() {

  'use strict';

  var apiUrl = 'http://87.4.5.140/api/v1/';
  var orgEnabled = '[29]';

  return {
    apiUrl: apiUrl,
    orgEnabled: orgEnabled
  };

});