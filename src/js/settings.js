define(function() {

  'use strict';

  var apiUrl = 'http://blackbox.puntacana.com/api/v1/';
  var orgEnabled = '[29]';

  return {
    apiUrl: apiUrl,
    orgEnabled: orgEnabled
  };

});