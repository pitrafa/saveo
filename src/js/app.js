/* jshint camelcase: false */
define([
  'jquery',
  'backbone',
  'views/app',
  'extensions'
], function(jq, Backbone, App){

  'use strict';

  var start = function() {

    new App();
  };

  return {
    start: start
  };

});