define([
  'underscore',
  'backbone',
  'settings'
], function(_, Backbone, settings){

  'use strict';

  var Propertys = Backbone.Collection.extend({

    url: settings.apiUrl + 'property' + '/',

  });

  return Propertys;
});