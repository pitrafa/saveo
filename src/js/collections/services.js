define([
  'underscore',
  'backbone',
  'settings'
], function(_, Backbone, settings){

  'use strict';

  var Services = Backbone.Collection.extend({

    url: settings.apiUrl + 'service' +'/',

  });

  return Services;
});