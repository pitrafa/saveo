define([
  'underscore',
  'backbone',
  'models/day',
  'settings'
], function(_, Backbone, Day, settings){

  'use strict';

  var Days = Backbone.Collection.extend({

    url: settings.apiUrl + 'day' + '/',

    model: Day,

    workingDays: function() {
      var filtered = this.filter(function(day){
        return day.get('day_type').holiday === false;
      });
      return new Days(filtered);
    },

    holiDays: function() {
      var filtered = this.filter(function(day){
        return day.get('day_type').holiday === true;
      });
      return new Days(filtered);
    }

  });

  return Days;
});