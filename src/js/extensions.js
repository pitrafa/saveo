/* jshint unused: false, camelcase: false */
define([
  'jquery',
  'underscore',
  'backbone',
  'settings',
  'validation',
  'stickit',
  'bootstrap-timepicker'
], function($, _, Backbone, settings){

  'use strict';

  var apiUrl = settings.apiUrl;

  Backbone.$.ajaxSetup({
    beforeSend: function(xhr, settings) {
      if (settings.url.startsWith(apiUrl)) {
        xhr.setRequestHeader('Authorization', 'Token ' + window.$token_api);
      }
    }
  });

  // Extend backbone model to perform custom validation.
  _.extend(Backbone.Model.prototype, Backbone.Validation.mixin);

  _.extend(Backbone.Validation.callbacks, {
    valid: function (view, attr, selector) {
    	var $el = view.$('[name=' + attr + ']'), 
      $group = $el.closest('.form-group');
        
      $group.removeClass('has-error');
      $group.find('.help-block').html('').addClass('hidden');
    },
    invalid: function (view, attr, error, selector) {
      var $el = view.$('[name=' + attr + ']'), 
      $group = $el.closest('.form-group');
     
      $group.addClass('has-error');
      $group.find('.help-block').html(error).removeClass('hidden');
    }
  });

});