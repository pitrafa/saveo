/* jshint camelcase: false */
define('jquery', [], function() {
  'use strict';
  return jQuery;
});

(function () {

  'use strict';

  var developing = true;

  require.config({

    baseUrl: 'js',

    paths: {
      underscore: '../../bower_components/underscore/underscore',
      backbone: '../../bower_components/backbone/backbone',
      handlebars: '../../bower_components/handlebars/handlebars',
      validation: '../../bower_components/backbone.validation/dist/backbone-validation-amd',
      stickit: '../../bower_components/backbone.stickit/backbone.stickit',
      raven: '../../bower_components/raven-js/dist/raven',
      'bootstrap-timepicker': '../../bower_components/bootstrap-timepicker/js/bootstrap-timepicker'
    },

    shim: {
      underscore: {
        deps: ['jquery'],
        exports: '_'
      },

      backbone: {
        deps: ['underscore', 'jquery'],
        exports: 'Backbone'
      },

      handlebars: {
        exports: 'Handlebars'
      }
    }

  });

  if (developing) {
    require.config({
      paths: {
        settings: 'settingsDev'
      }
    });
  }

  require([
    'app',
    'raven'
  ], function(app, Raven){
    if (!developing) {
      Raven.config(window.$sentry_credentials).install();
    }
    app.start();
  });

})();