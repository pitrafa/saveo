/* jshint camelcase: false */
define([
  'jquery',
  'underscore',
  'backbone',
  'templates',
], function($, _, Backbone, templates){

  'use strict';

  var RequestCreate = Backbone.View.extend({

    template: templates.requestcreate,

    events: {
      'submit': 'save',
      'show.bs.modal': 'setup'
    },

    // Set the bindings for input elements with model properties.
    bindings: {
      '#service': 'service',
      '#note': 'note',
      '[name=require_quotation]': 'require_quotation',
      '#property': '_property',
      '#phone': 'phone',
      '#email': 'email',
      '[name=days]': 'days',
      '#checkin_wd': 'checkin_wd',
      '#checkout_wd': 'checkout_wd',
      '#checkin_hd': 'checkin_hd',
      '#checkout_hd': 'checkout_hd',

      '#user': 'user',
      '#sap_customer': 'sap_customer',
      '#ticket_id': 'ticket_id'
    },

    initialize: function(options) {
      Backbone.Validation.bind(this);

      this.user = options.user;
      this.resident = options.resident;
      this.client = options.client;
      this.services = options.services;
      this.propertys = options.propertys;
      this.days = options.days;

      this.listenTo(this.client, 'sync', this.setClient, this);
      this.listenTo(this.model, 'change:service', this.setScheduler, this);
      this.listenTo(this.model, 'change:days', this.setSelectedDays, this);

      // Set sap customer
      this.setSapCustumer(this.resident);
    },

    render: function() {
      this.$el.html(this.template({
        user:this.user.toJSON(), resident:this.resident.toJSON(),
        services:this.services.toJSON(), propertys:this.propertys,
        days: this.days.toJSON(), workingDays: this.days.workingDays().toJSON(),
        holidays: this.days.holiDays().toJSON()
      }));

      // Bind input elements with model properties.
      this.stickit();

      // Cache DOM elements for easy access.
      this.$service = this.$('#service');
      this.$submit = this.$('#submit');
      this.$scheduler = this.$('#scheduler');
      this.$selectedWorkinDays = this.$('#selected-workingdays');
      this.$selectedHoliDays = this.$('#selected-holidays');

      // Set checkin and checkout
      this.setHour();

      // Initialize timepicker
      this.$('.timepicker').timepicker();

      return this;
    },

    setup: function() {
      var help_topic = $('td > a[href*="/tickets?help-topic"]').text().trim();
      var service = this.$('#service option:contains('+ help_topic +')').val();
      this.$service.val(service).trigger('change');
      
      this.$service.attr('disabled', 'disabled');
    },

    setSapCustumer: function(model) {
      var sap_customer = model.get('sap_customer');
      this.model.set({
        sap_customer:sap_customer
      });

      // Fetch client
      this.client.set({codigo:sap_customer});
      this.client.fetch();
    },

    setClient: function(model) {
      this.model.set({
        email: model.get('e_mail'),
        phone: model.get('telefono')
      });
    },

    setScheduler: function(model, value) {
      if (value !== '') {
        var serviceType = this.services.get(value);
        if (serviceType.get('scheduled')) {

          this.$scheduler.removeClass('hidden');
          this.model.set({scheduled:true});
        } else {

          this.$scheduler.addClass('hidden');
          this.model.set({scheduled:false});
        }
      } else {

        this.$scheduler.addClass('hidden');
        this.model.set({scheduled:false});
      }
    },

    setSelectedDays: function(model, values) {
      var selectedWd = [];
      var selectedHd = [];
      for (var x in values) {
        var val = values[x];
        var day = this.days.get(val);
        if (day.get('day_type').holiday) {
          selectedHd.push(day.get('name'));
        } else {
          selectedWd.push(day.get('name'));
        }
      }
      var selectedWdStr = selectedWd.toString().replace(/,/g, ', ') || '&nbsp;';
      var selectedHdStr = selectedHd.toString().replace(/,/g, ', ') || '&nbsp;';
      this.$selectedWorkinDays.html(selectedWdStr);
      this.$selectedHoliDays.html(selectedHdStr);
    },

    setHour: function() {
      var workinDay = this.days.workingDays().first();
      var holiday = this.days.holiDays().first();
      this.model.set({
        checkin_wd: workinDay.get('day_type').schedule_availability.start_time,
        checkout_wd: workinDay.get('day_type').schedule_availability.end_time,
        checkin_hd: holiday.get('day_type').schedule_availability.start_time,
        checkout_hd: holiday.get('day_type').schedule_availability.end_time
      });
    },

    save: function(e) {
      e.preventDefault();

      if (!this.model.isValid(true)){
        return;
      }
      
      // Disable the submit button to prevent multiple submits.
      this.$submit.attr('disabled', 'disabled');

      this.model.save(null, {
        success: function () {
          window.location.reload();
        },
        error: function (model, response) {
          console.log(response);
        }
      });
    }

  });

  return RequestCreate;
});