define([
  'jquery',
  'underscore',
  'backbone',
  'templates',
], function(jq, _, Backbone, templates){

  'use strict';

  var IndexView = Backbone.View.extend({

    template: templates.index,

    render: function() {
      this.$el.html(this.template(this.model.toJSON()));
      return this;
    },

  });

  return IndexView;
});