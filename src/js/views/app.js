/* jshint camelcase: false */
define([
  'jquery',
  'backbone',
  'models/aviso',
  'models/userPortal',
  'models/resident',
  'models/servicerequest',
  'models/client',
  'collections/services',
  'collections/days',
  'views/requestcreate',
  'views/requestupdate',
  'templates',
], function(jq, Backbone, Aviso, User, Resident, ServiceRequest, Client,
  Services, Days, RequestCreate, RequestUpdate, templates){

  'use strict';

  var email, user, resident, ticket_id, aviso, serviceRequest, client;
  var services, propertys, days;
  
  var App = Backbone.View.extend({

    initialize: function() {
      /**
       * Vemos si el requester tiene usuario en el portal
       * si no tiene no continuamos y lo informamos.
       */
      user = this.getUser(this.getEmail());
      if (!user.get('id')) {
        return this.noUser(user);
      }

      /**
       * Seteamos la clasificacion del requester, cliente sap.
       */
      resident = new Resident({id:user.get('resident')});
      resident.fetch({async:false});
      this.setClientClasification(resident.get('sap_customer'));

      /**
       * El requester tiene usuario en el portal
       * vemos si el numero de ticket tiene un service
       * request en el portal
       */
      ticket_id = window.$ticket_id;
      serviceRequest = this.getServiceRequest(ticket_id);
    },

    showView: function(view) {
      view.setElement(jq('#customer_info'));
      view.render();
    },

    getEmail: function() {
      email = jq('td > a[href*="/user/"]:contains("@")').first().text().trim();
      return email;
    },

    getUser: function(email) {
      user = new User({email:email});
      user.fetch({async: false});
      return user;
    },

    setClientClasification: function(codigoCliente) {
      var client = new Client({codigo:codigoCliente});
      client.fetch({
        success: function(model) {
          var clasification = model.get('clasification_name');
          var selector = '#side-bar > li:nth-child(3) > a';
          $(selector).append(
            '<br> <i class="fa fa-shield"></i><b> ' + clasification + '</b>');
        }
      });
    },

    getServiceRequest: function(ticket_id) {
      var that = this;
      aviso = new Aviso({ticket_id:ticket_id});
      aviso.getServiceRequest({
        success: function(data) {
          if (data.count === 1) {
            aviso.fetch({async:false});
            var id = data.results[0].id;
            serviceRequest = new ServiceRequest({id:id});
            serviceRequest.fetch({
              success: function(model) {
                that.showView(new RequestUpdate({model:model, user:user, aviso:aviso}));
              }
            });

          }
          if (data.count === 0) {
            client = new Client();
            propertys = resident.get('properties');
            services = new Services();
            days = new Days();

            // Create service request to pass to the view.
            serviceRequest = new ServiceRequest({
              user:user.get('id'), ticket_id:ticket_id});

            // fetch service request async:false
            services.fetch({async:false});

            // fetch days and create the new request view when fetch success.
            days.fetch({
              success: function() {
                that.showView(new RequestCreate({
                  model:serviceRequest, user:user, resident:resident,
                  propertys: propertys,
                  client:client, services:services, days:days
                }));
              }
            });
          }
        }
      });
    },

    noUser: function() {
      jq('#customer_info').html(templates.requestupdate({user:user.toJSON()}));
    }

  });

  return App;
});