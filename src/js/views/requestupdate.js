/* jshint camelcase: false */
define([
  'jquery',
  'underscore',
  'backbone',
  'templates',
], function(jq, _, Backbone, templates){

  'use strict';

  var RequestUpdate = Backbone.View.extend({

    template: templates.requestupdate,
    propertyTpl: templates.property,

    events: {
      'click .btn-aviso': 'createAviso',
      'click .approve-quotation': 'approveQuotation',
      'click .reject-quotation': 'rejectQuotation',
      'click .approve-work': 'approveWork',
      'click .reject-work': 'rejectWork',
    },

    initialize: function(options) {
      if(!this.model) {
        throw new Error('model is required');
      }

      this.user = options.user;
      this.aviso = options.aviso;
    },

    setProperty: function(model) {
      this.$property.html(this.propertyTpl(model.toJSON()));
    },

    render: function() {
      this.$el.html(this.template({
        user:this.user.toJSON(), request:this.model.toJSON(),
        aviso:this.aviso.toJSON()
      }));

      this.$property = this.$('#property');

      this.setProperty(this.model);

      // Inicializamos el popover
      jq('[data-toggle="popover"]').popover();

      return this;
    },

    createAviso: function(e) {
      e.preventDefault();
      this.aviso.save(null, {
        success: function(){
          window.location.reload();
        },
        error: function(){
          window.alert('Error creando aviso!');
        }
      });
    },

    approveQuotation: function(e) {
      e.preventDefault();
      this.model.approveQuotation({
        success: function() {
          window.location.reload();
        },
        error: function() {

        }
      });
    },

    rejectQuotation: function(e) {
      e.preventDefault();
      this.model.rejectQuotation({
        success: function() {
          window.location.reload();
        },
        error: function() {

        }
      });
    },

    approveWork: function(e) {
      e.preventDefault();
      this.model.approveWork({
        success: function() {
          window.location.reload();
        },
        error: function() {

        }
      });
    },

    rejectWork: function(e) {
      e.preventDefault();
      this.model.rejectWork({
        success: function() {
          window.location.reload();
        },
        error: function() {

        }
      });
    }

  });

  return RequestUpdate;
});