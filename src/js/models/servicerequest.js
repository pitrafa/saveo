/* jshint camelcase: false */
define([
  'underscore',
  'backbone',
  'settings'
], function(_, Backbone, settings){

  'use strict';

  var ServiceRequest = Backbone.Model.extend({

    url: function() {
      var url = settings.apiUrl + 'service-request' + '/';
      if (this.get('id')) {
        url += this.get('id') + '/';
      } else {
        url += 'faveo/';
      }
      return url;
    },

    validation: {
      service: {
        required: true
      },
      require_quotation: {
        required: true
      },
      _property: {
        required: true
      },
      phone: {
        required: true
      },
      email: {
        required: true,
        pattern: 'email'
      },
      days: function(value){
        // if the the scheduled attr is set to true
        // then the days may not be empty.
        if (this.get('scheduled') && value === null){
          return 'Select the day';
        }
      },
      checkin_wd: {
        required: true
      },
      checkout_wd: {
        required: true
      },
      checkin_hd: {
        required: true
      },
      checkout_hd: {
        required: true
      }
    },

    approveQuotation: function(opts) {
      var url = this.url() + 'approve-quotation/';
      var options = {
        url:  url,
        type: 'POST'
      };
      _.extend( options, opts);
      return Backbone.sync.call( this, null, this, options);
    },

    rejectQuotation: function(opts) {
      var url = this.url() + 'reject-quotation/';
      var options = {
        url:  url,
        type: 'POST'
      };
      _.extend( options, opts);
      return Backbone.sync.call( this, null, this, options);
    },

    approveWork: function(opts) {
      var url = this.url() + 'approve-work/';
      var options = {
        url:  url,
        type: 'POST'
      };
      _.extend( options, opts);
      return Backbone.sync.call( this, null, this, options);
    },

    rejectWork: function(opts) {
      var url = this.url() + 'reject-work/';
      var options = {
        url:  url,
        type: 'POST'
      };
      _.extend( options, opts);
      return Backbone.sync.call( this, null, this, options);
    },

    fields_scheduled: [
      'user', 'ticket_id', 'sap_customer', 'service', 'note',
      'require_quotation', '_property', 'phone', 'email',
      'date_service_request'
    ],

    fields_no_scheduled: [
      'user', 'ticket_id', 'sap_customer', 'service', 'note',
      'require_quotation', '_property', 'phone', 'email'
    ],

    toJSON: function() {
      var json = _.clone(this.attributes);
      if (this.get('id')){
        return json;
      }

      var fields;
      if (this.get('scheduled')) {
        fields = this.fields_scheduled;
        var days = this.get('days');
        var result = [];

        for (var x in days) {
          result.push(days[x]);
        }
        json.date_service_request = {};
        json.date_service_request.day = result;
        json.date_service_request.checking = this.get('checkin_wd');
        json.date_service_request.checkout = this.get('checkout_wd');
  
      } else {
        fields = this.fields_no_scheduled;
      }

      for(var attr in json) {
        if((json[attr] instanceof Backbone.Model) || (json[attr] instanceof Backbone.Collection)) {
          json[attr] = json[attr].toJSON();   
        }
      }

      return _.pick(json, fields);
    }

  });

  return ServiceRequest;
});