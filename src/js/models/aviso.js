define([
  'underscore',
  'backbone',
  'settings'
], function(_, Backbone, settings){

  'use strict';

  var url;

  var Aviso = Backbone.Model.extend({

    url: function() {
      if (this.get('is_new')) {
        url = settings.apiUrl + 'aviso/';
      } else {
        url = settings.apiUrl + 'aviso/?ticket_id=' + this.get('ticket_id');
      }
      return url;
    },

    isRACO: function() {
      return this.get('estado_orden') === 'RACO';
    },

    isRACE: function() {
      return this.get('estado_aviso') === 'RACE';
    },

    getServiceRequest: function(opts) {
      var url = settings.apiUrl + 'service-request/?ticket_id=' + this.get('ticket_id');
      var options = {
        url:  url,
        type: 'GET'
      };
      _.extend( options, opts);
      return Backbone.sync.call(this, null, this, options);
    },

    toJSON: function() {
      var json = _.clone(this.attributes);
      json.isRACO = this.isRACO();
      json.isRACE = this.isRACE();
      return json;
    }

  });

  return Aviso;
});