define([
  'underscore',
  'backbone',
  'settings'
], function(_, Backbone, settings){

  'use strict';

  var Resident = Backbone.Model.extend({

    url: function() {
      return settings.apiUrl + 'resident/' + this.get('id') + '/';
    }

  });

  return Resident;
});