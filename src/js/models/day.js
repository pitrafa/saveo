define([
  'underscore',
  'backbone',
  'settings'
], function(_, Backbone, settings){

  'use strict';

  var Day = Backbone.Model.extend({

    url: settings.apiUrl + 'day' + '/',

  });

  return Day;
});