define([
  'underscore',
  'backbone',
  'settings'
], function(_, Backbone, settings){

  'use strict';

  var Client = Backbone.Model.extend({

    url: function() {
      return settings.apiUrl + 'client-info/?client=' + this.get('codigo');
    }

  });

  return Client;
});