define([
  'underscore',
  'backbone',
  'settings'
], function(_, Backbone, settings){

  'use strict';

  var UserPortal = Backbone.Model.extend({

    url: function() {
      return settings.apiUrl + 'users/?email=' + this.get('email');
    },

    parse: function(response) {
      return response[0];
    },

  });

  return UserPortal;
});