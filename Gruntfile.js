/* jshint camelcase: false */
module.exports = function(grunt) {

  'use strict';

  require('load-grunt-tasks')(grunt);

  var serveStatic = require('serve-static');

  var config = {
    src: 'src',                                 // working directory
    tests: 'tests',                             // unit tests folder
    dist: 'dist',                               // distribution folder
    platform: 'web'                             // current target platform
  };


  grunt.initConfig({

    config: config,

    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      gruntfile: 'Gruntfile.js',
      src: ['<%= config.src %>/js/**/*.js', '!<%= config.src %>/js/templates.js']
    },

    // Run jasmine tests.
    karma: {
      unit: {
        basePath: '',
        singleRun: false,
        frameworks: ['jasmine-jquery', 'jasmine', 'requirejs', 'sinon'],
        files: [
          {
            src: 'bower_components/**/*.js',
            included: false
          },
          {
            src: '<%= config.src %>/**/*.js',
            included: false
          },
          {
            src: '<%= config.tests %>/*spec.js',
            included: false
          },
          {
            src: '<%= config.tests %>/main.js'
          },
          {
            src: 'tests/mixins/*.js',
            included: false
          },
        ],
        exclude: ['<%= config.src %>/main.js'],
        port: 9002,
        reporters: ['mocha', 'coverage'],
        preprocessors: {
          '<%= config.src %>/js/views/*.js': 'coverage'
        },
        coverageReporter: {
          type: 'text'
        },
        plugins: [
          'karma-phantomjs-launcher',
          'karma-chrome-launcher',
          'karma-jasmine-jquery',
          'karma-jasmine',
          'karma-requirejs',
          'karma-coverage',
          'karma-mocha-reporter',
          'karma-sinon'
        ],
        browsers: ['PhantomJS']
      }
    },
      
    handlebars: {
      compile: {
        options: {
          amd: true,
          processName: function (filepath) {
            var pieces = filepath.split('/');
            return pieces[pieces.length - 1].split('.')[0];
          }
        },
        src: ['<%= config.src %>/html/{,*/}*.handlebars'],
        dest: '<%= config.src %>/js/templates.js'
      }
    },

    connect: {
      options: {
        hostname: 'localhost',
        open: true,
        livereload: true
      },
      app: {
        options: {
          middleware: function (connect) {
            return [
              serveStatic(config.src),
              connect().use('/bower_components', serveStatic('./bower_components'))
            ];
          },
          port: 9000,
          open: {
            target: 'http://localhost:9000/index.<%= config.platform %>.html'
          }
        }
      }
    },

    watch: {
   
      // Watch grunt file.
      gruntfile: {
        files: ['Gruntfile.js'],
        tasks: ['jshint:gruntfile']
      },
     
      // Watch javascript files.
      js: {
        files: [
          '<%= config.src %>/js/**/*.js',
          '!<%= config.src %>/js/templates.js'
        ],
        tasks: ['jshint:src'],
        options: {
          livereload: true
        }
      },
     
      // Watch handlebar templates.
      handlebars: {
        files: [
          '<%= config.src %>/html/{,*/}*.handlebars'
        ],
        tasks: ['handlebars'],
        options: {
          livereload: true
        }
      },
     
      // Watch html and css files.
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          '<%= config.src %>/index.<%= config.platform %>.html',
          '<%= config.src %>/css/safe.css',
          '<%= config.src %>/css/safe.<%= config.platform %>.css'
        ]
      }
    },

    clean: {
      options: {
        force: true
      },
      dist: ['<%= config.dist %>/']
    },

    requirejs: {
      compile: {
        options: {
          baseUrl: '<%= config.src %>/js',
          mainConfigFile: '<%= config.src %>/js/main.js',
          almond: true,
          include: ['main'],
          out: '<%= config.dist %>/saveo.min.js',
          optimize: 'uglify'
        }
      }
    },

    // SSH deploy
    secret: grunt.file.readJSON('./secret.json'),
    environments: {
      options: {
        local_path: 'dist',
        current_symlink: 'current',
        deploy_path: '/home/integra/apps/gapp/saveo'
      },
      dev: {
        options: {
          host: '<%= secret.dev.host %>',
          username: '<%= secret.dev.username %>',
          password: '<%= secret.dev.password %>',
          port: '<%= secret.dev.port %>',
          releases_to_keep: '1',
          release_subdir: 'saveo'
        }
      }
    },

  });

  grunt.registerTask('serve', [
    'connect',
    'watch'
  ]);

  grunt.registerTask('buildweb', [
    'jshint',
    'clean',
    'handlebars',
    'requirejs'
  ]);

  grunt.registerTask('test', [
    'jshint',
    'handlebars',
    'karma'
  ]);

  grunt.registerTask('default', [
    'serve'
  ]);

};