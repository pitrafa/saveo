# Saveo (Version 0.1.0)

## Getting Started

This plugin is meant to be used inside the faveo service desk, was developed to be called with the script tag and src, inside the 'Custom Code Snippet', to make the plugin work well we need put the falowing:

```html
<script>var $token_api = "token-to-the-api-been-called";</script>
<script>var $sentry_credentials = "our_sentry_credentials";</script>
<script src="http://your-cdn/saveo.min.js"></script>
```

### Usage
When the plugin is activated in faveo and we has declared the var $token_api (The token of the api we will be calling) an element is created in the div with id 'customer_info', if the currect ticket does not have aviso in SAP a dropdown menu let us create a new aviso, or if the ticket has an aviso in SAP a table will be draw with the info about the aviso and the orden if has one.

## Release History
14/09/2018 - v0.1.0 - Initial release.

## Tests

All test are failing, new to rewrite all of then, some behave changes ex. Now first check if the requestes has a user in the portal, that the first tip.
